package tinet.tutoriales;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class FactorialTest {

	private static final long[][] DATOS_DE_PRUEBA = { { 0, 1 }, { 1, 1 }, { 2, 2 }, { 3, 6 }, { 4, 24 },
			{ 10, 3628800 } };

	private Factorial sut;

	@Before
	public void init() {
		sut = new Factorial();
	}

	@Test
	public void debeRetornarElValorFactorialCorrecto() {
		for (long[] valores : DATOS_DE_PRUEBA) {
			Assert.assertEquals(valores[1], sut.factorial(valores[0]));
		}
	}
}
