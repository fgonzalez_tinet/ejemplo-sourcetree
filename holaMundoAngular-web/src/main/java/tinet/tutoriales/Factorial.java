package tinet.tutoriales;

public class Factorial {

	public long factorial(long i) {
		if (i < 2) {
			return 1;
		} else {
			return factorial(i - 1) * i;
		}
	}

}
